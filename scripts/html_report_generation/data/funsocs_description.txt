Name	Description
disable_organ	Disables an organ, but not necessarily by killing individual cells. This includes neurotoxins that block receptor conductance, superantigens, and toxins that degrade lung function, gut function, or are involved in the disruption of blood homeostasis.
cytotoxicity	Kills cells by inhibiting a vital process such as translation, directly lysing the cells through pore formation, or destabilizing the plasma membrane
degrade_ecm	Mediates the loosening of host tissue connectivity including reducing cell-to-cell adherence and cell-to-substrate adherence as well as proteolytic destruction of the host extracellular matrix (ECM)
induce_inflammation	Directly activate host inflammatory pathways to cause damage
bacterial_counter_signaling	Bacterial suppression of host immune signaling within host cells to avoid inflammatory responses
viral_counter_signaling	Viral suppression of host immune signaling within host cells to avoid inflammatory responses
resist_complement	Enable resistance from host complement components
counter_immunoglobulin	Binds, sequesters, destroys, or otherwise neutralizes host immunoglobulin
plant_rna_silencing	Counters host plant defenses by triggering the degradation of specific sequences, pre-translation
resist_oxidative	Enable resistance of host oxidative killing
suppress_detection	Viral interference with host immune components involved in processing or display of parasite antigens on the surface of host cells
avirulence_plant	Virulence effector proteins of plant pathogens that typically suppress immune signaling of the host plant
host_gtpase	Target host small GTPases
host_transcription	Target host transcription to inhibit or activate
host_translation	Target host translation, usually to inhibit
host_ubiquitin	Target host ubiquitination machinery
host_xenophagy	Target host xenophagy/autophagy
nonviral_invasion	Mediates bacterial or eukaryotic pathogen invasion into host cells
viral_invasion	Mediates viral invasion into host cell
viral_movement	Enable movement of viral parasite within a host cell or tissue
virulence_activity	Inclusive of a wide variety of virulence activities, including those that did not fit under another FunSoC category
host_cell_cycle	Target host cell cycle component or induce alterations in the host cell cycle
host_cell_death	Target host apoptotic cell death pathways either to inhibit or activate
host_cytoskeleton	Target host cytoskeletal components or induce alterations in the host cytoskeleton
secreted_effector	Secreted bacterial effectors of unknown function
antibiotic_resistance	Counters the effect of antibiotics administered to inhibit the growth or vital functioning of bacterial or eukaryotic parasites. Note: These are not specific to pathogens.
develop_in_host	Involved in the growth or development of a bacterial or eukaryotic parasite within the host
nonviral_adhesion	Mediates adherence of nonviral parasites to host cells
secretion	Bacterial secretion system components (T1SS - T8SS), including chaperones
toxin_synthase	Enzymes that synthesize or modify toxins, particularly mycotoxins
viral_adhesion	Mediates viral adherence to host cells
virulence_regulator	Bacteria or eukaryotic parasites transcription factor of virulence
