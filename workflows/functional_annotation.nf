#!/usr/bin/env nextflow

params.fasta = ''
params.working = ''
params.databases = ''
params.threads = 1
params.log = '/dev/null'
params.help = false
params.slurm = false
params.ancestral = false
params.sensitive = false
params.bitscore_cutoff = 5
params.evalue = ""
ancestral_flag = ""
include_cc_flag = ""
params.includecc = false


Executor = 'local'

def usage() {
    log.info ''
    log.info 'Usage: nextflow run functional_annotation.nf --fasta /Path/to/infile.fasta --working /Path/to/working_directory --databases /Path/to/databases [--evalue 10] [--threads 4] [--slurm] [--log=/Path/to/run.log]'
    log.info '  --fasta     Path to the input NT FASTA file'
    log.info '  --working   Path to the output working directory'
    log.info "  --databases Path to the SEQSCREEN database directory"
    log.info "  --evalue    E value cut-off (Default=30)"
    log.info "  --threads   Number of threads to use (Default=1)"
    log.info '  --slurm     Submit modules in this workflow to run on a SLURM grid (Default = run locally)'
    log.info "  --ancestral Include all ancestral GO terms" 
    log.info "  --includecc     Include cellular component go terms"
    log.info "  --bitscore_cutoff Tiebreak across all uniprots within this % of the top bitscore"
    log.info "  --sensitive Run in sensitive mode"
    log.info "  --log       Where to write the log file to (Default=No log)"
    log.info '  --help      Print this help message out'
    log.info ''
    exit 1
}

if (params.help) {
    usage()
}

if (params.evalue) {
    EVALUE = params.evalue
} else {
    if (params.sensitive) {
        EVALUE = 10
    } else {
        EVALUE = 30
    }
}

if (params.slurm) {
    Executor = 'slurm'
}

if (!params.fasta) {
    log.error "Missing argument for --fasta option"
    usage()
}

if (params.includecc) {
    include_cc_flag = "--include-cc"
}

if (!params.working) {
    log.error "Missing argument for --working option"
    usage()
}
if (params.ancestral) {
    ancestral_flag = "--ancestral"
}

if (!params.databases) {
    log.error "Missing argument for --databases option"
    usage()
}


fastafile = file(params.fasta)
workingDir = file(params.working)
databasesDir = file(params.databases)
logfile = file(params.log)

BASE=fastafile.getName()
MODULES =  "$workflow.projectDir/../modules"
SCRIPTS = "$workflow.projectDir/../scripts"
THREADS=params.threads
WORKFLOW="functional_annotation"

mummerDir = file("$workingDir/$WORKFLOW/mummer")
megaresDir = file("$workingDir/$WORKFLOW/megares")
fxnDir = file("$workingDir/$WORKFLOW/functional_assignments")

translatedFile = "$workingDir/initialize/six_frame_translation/${BASE}.translated.fasta"

process Create_Working_Directories {
    output:
    stdout create

    script:
    if (params.sensitive) {
        """
        mkdir -p $workingDir/$WORKFLOW
        if [ -d $mummerDir ]; then rm -rf $mummerDir; fi;
        if [ -d $megaresDir ]; then rm -rf $megaresDir; fi;
        if [ -d $fxnDir ]; then rm -rf $fxnDir; fi;
        mkdir $fxnDir
        mkdir $mummerDir
        mkdir $megaresDir
        """
    } else {
        """
        mkdir -p $workingDir/$WORKFLOW
        if [ -d $fxnDir ]; then rm -rf $fxnDir; fi;
        mkdir $fxnDir
        """
    }

}

process Initialize {
    input:
    val create from create

    output:
    stdout init

    executor Executor
    
    script:
    """
    echo -n " # Launching $WORKFLOW workflow ...... " | tee -a $logfile; date '+%H:%M:%S %Y-%m-%d' | tee -a $logfile
    """
}

process mummer {
    input:
    val init from init

    output:
    stdout mummer

    if ( Executor == 'local' ) {
       executor "local"
    }

    else if ( Executor == 'slurm' ) {
       clusterOptions "--ntasks-per-node $THREADS"
       executor "slurm"
    }
    
    script:
    if (params.sensitive) {
        """
        echo ${params.sensitive} > log.txt
        ${MODULES}/mummer.sh --fasta=$fastafile \
                             --database=${databasesDir}/rebase/rebase.fna \
                             --out=$mummerDir/${BASE}.mummer_re.txt
        """
    } else {
        """:"""
    }
}

process megares {
    input:
    val init from init

    output:
    stdout megares

    if ( Executor == 'local' ) {
       executor "local"
    }

    else if ( Executor == 'slurm' ) {
       clusterOptions "--ntasks-per-node $THREADS"
       executor "slurm"
    }

    script:
    if (params.sensitive) {
        """
        echo ${params.sensitive} > $megaresDir/log.txt
        ${MODULES}/blastn.sh --fasta=$fastafile \
                             --database=${databasesDir}/megares/megares_full \
                             --out=$megaresDir/${BASE}.megares \
                             --threads=${THREADS} \
                             --evalue=${EVALUE}
        """
    } else { """:""" }
}


process functional_assignments {
    input:
    val init from init
    val mummer from mummer
    val megares from megares

    output:
    stdout functional_assignments

    executor Executor
    
    """
    while [ ! -f $workingDir/taxonomic_identification/blastx/${BASE}.ur100.btab ]
    do
      sleep 5
    done

    ${SCRIPTS}/functional_report_generation.py --fasta=${fastafile} \
                                               --urbtab=$workingDir/taxonomic_identification/blastx/${BASE}.ur100.btab \
                                               --go=${databasesDir}/go/go_network.txt \
                                               --out=${workingDir}/$WORKFLOW/functional_assignments/functional_results.txt \
                                               --annotation=${databasesDir}/annotation_scores.pck \
                                               $ancestral_flag \
                                               $include_cc_flag \
                                               --cutoff=${params.bitscore_cutoff} > ${workingDir}/$WORKFLOW/functional_assignments.log 2>&1
    echo -n " # $WORKFLOW workflow complete ....... " | tee -a $logfile; date '+%H:%M:%S %Y-%m-%d' | tee -a $logfile
    """
}


create.subscribe { print "$it" }
init.subscribe { print "$it" }
mummer.subscribe { print "$it" }
megares.subscribe { print "$it" }
functional_assignments.subscribe { print "$it" }
