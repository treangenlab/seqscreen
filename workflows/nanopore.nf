#!/usr/bin/env nextflow

params.fasta = ""
params.working = ""
params.databases = ""
params.threads = 1
params.evalue = 0.001
params.splitby = 100000
params.taxonomy_confidence_threshold = 0
params.bitscore_cutoff = false
params.log = '/dev/null'
params.help = false
params.slurm = false
params.ancestral = false
params.includecc = false
params.skip_report = false
params.online = false
params.format = 1
params.keep_html_ont = false
params.filter_taxon = "\"\""
params.keep_taxon = "\"\""
ancestral_flag = ""
include_cc_flag = ""
params.prefix = ''
cutoff_flag = "--cutoff=5"
online_flag = ""
mode="ont"

Executor = 'local'

MODULES =  "$workflow.projectDir/../modules"
SCRIPTS =  "$workflow.projectDir/../scripts"


def usage() {
    log.info ""
    log.info "Usage: nextflow run nanopore.nf --fasta /Path/to/infile.fasta --databases /Path/to/databases --working /Path/to/working_directory [--evalue 10] [--threads 4] [--slurm]  [--log /Path/to/log.txt]"
    log.info "  --fasta     Path to the input NT FASTA file"
    log.info "  --databases Path to the database directory"
    log.info "  --working   Path to the output working directory"
    log.info "  --evalue    E value cut-off (Default=10)"
    log.info "  --threads   Number of threads to use (Default=1)"
    log.info "  --splitby   Number of fasta sequences in each chunk"
    log.info "  --slurm     Submit modules in this workflow to run on a SLURM grid (Default = run locally)"
    log.info "  --ancestral Include all ancestral GO terms" 
    log.info "  --includecc     Include cellular component go terms"
    log.info "  --bitscore_cutoff Tiebreak across all uniprots within this % of the top bitscore"
    log.info "  --prefix   Add argument to beginning of the output files"
    log.info "  --format        Format type: [1] Original, [2] Hits only, [3] FunSoC only, [4] Gene-Centric,[5] Gene-Centric FunSoC Only [Default: 1]"
    log.info "  --skip_report   Skip report generation step and only generate intermediate files" 
    log.info "  --online        Pull reference genomes from NCBI for reference_inference [Needs web access]"
    log.info "  --filter_taxon  Filter comma separated list of taxon"
    log.info "  --keep_taxon    Keep comma separated list of taxon"
    log.info "  --taxonomy_confidence_threshold Confidence threshold for multi-tax ids (Average) [Default: 0.0]"
    log.info "  --keep_html_ont Keep html report in ont mode [Takes additional memory]" 
    log.info "  --version   Verison number for the seqscreen pipeline"
    log.info "  --log       Where to write log file (Default=no log)"
    log.info "  -h, --help  Print this help message out"
    log.info ""
    exit 1
}


if (params.help) {
    usage()
}

if (params.slurm) {
    Executor = 'slurm'
}

if (!params.fasta) {
    log.error "Missing argument for --fasta option"
    usage()
}

if (!params.working) {
    log.error "Missing argument for --working option"
    usage()
}

if (!params.databases) {
    log.error "Missing argument for --databases option"
    usage()
}
rflag = 'ont'
if (params.bitscore_cutoff) {
    cutoff_flag = "--cutoff=${params.bitscore_cutoff}"
    if(rflag) {
        rflag = rflag + ",bitscore:" + params.bitscore_cutoff
    }else{
        rflag = "bitscore:" + params.bitscore_cutoff
    }

}

if (params.evalue != "") {
   if(rflag) {
       rflag = rflag + ",evalue:" + params.evalue
   }
   else {
       rflag = "evalue:" + params.evalue
   }
}


if (params.includecc) {
    include_cc_flag = "--include-cc"
    if(rflag){
        rflag = rflag + ",includecc"
    }else{
        rflag = 'includecc'
    }

}

if (params.ancestral) {
    ancestral_flag = "--ancestral"
    if(rflag) {
      
       rflag = rflag + ",ancestral"
    }
   else {
       rflag = "ancestral"
   }
}

if (params.splitby != ""){
    if(rflag){
        rflag = rflag + ",splitby:" + params.splitby
    }else{
        rflag = "splitby:" + params.splitby
    }
}

if(! rflag)
{
   rflag = "None"
}
 
if (params.online) {
   online_flag = "--online"
}

if (params.taxonomy_confidence_threshold) {
   taxonomy_confidence_threshold_flag = "--taxonomy_confidence_threshold ${params.taxonomy_confidence_threshold}"
}

fastafile = file(params.fasta)
workingDir = file(params.working)
databaseDir = file(params.databases)
versionNum = params.version
logfile = file(params.log)

BASE = fastafile.getName()
THREADS = params.threads
EVALUE = params.evalue
WORKFLOW = "taxonomic_identification"
FWORKFLOW="functional_annotation"
RWORKFLOW="report_generation"

blastnDir = file("$workingDir/$WORKFLOW/blastn")
blastxDir = file("$workingDir/$WORKFLOW/blastx")
taxDir = file("$workingDir/$WORKFLOW/taxonomic_assignment")
fxnDir = file("$workingDir/$FWORKFLOW/functional_assignments")


process Create_Working_Directories {
    output:
    stdout create

    """
    mkdir -p $workingDir/$WORKFLOW/
    mkdir -p $workingDir/$FWORKFLOW
    mkdir -p $workingDir/$RWORKFLOW
    if [ -d $blastnDir ]; then rm -rf $blastnDir; fi;
    if [ -d $blastxDir ]; then rm -rf $blastxDir; fi;
    if [ -d $taxDir ]; then rm -rf $taxDir; fi;    
    if [ -d $fxnDir ]; then rm -rf $fxnDir; fi; 
    
    mkdir $blastnDir
    mkdir $blastxDir
    mkdir $taxDir
    mkdir $fxnDir
    """
}

process Initialize {
    input:
    val create from create

    output:
    stdout init

    executor Executor

    """
    echo -n " # Launching $WORKFLOW workflow ...... " | tee -a $logfile; date '+%H:%M:%S %Y-%m-%d' | tee -a $logfile
    """
}

Channel
    .fromPath(fastafile)
    .splitFasta(by: params.splitby, file: true)
    .set { fasta_ch }

process diamond {

    input:
    val	init from init
    file 'chunk' from fasta_ch

    output:
    file 'outchunk.ur100.btab' into diamondBtabResults
    file 'outchunk.ur100.xml' into diamondXmlResults
    file 'diamond.log' into diamondLogResults
    stdout diamond_std into diamond_wait

    if ( Executor == 'local' ) {
       executor "local"
       maxForks 1
    }

    else if ( Executor == 'slurm' ) {
       clusterOptions "--ntasks-per-node $THREADS"
       executor "slurm"
    }

    script:
    """
    echo "running diamond on ${chunk}"

    ## Run the BLASTx and produce an ASN file
    diamond blastx -q chunk \
        -d "${databaseDir}/diamond/uniref.mini.dmnd" \
        -o outchunk \
        --evalue "${EVALUE}" \
        --threads "${THREADS}" \
        --block-size 4 \
        --index-chunks 3 \
        --salltitles \
        --fast \
        --frameshift 15 \
        --range-culling \
        --culling-overlap 50 \
        --range-cover 50 \
        --min-orf 30 \
        -k 1 \
        --masking seg \
        -f 100 \
        --log
        ${SCRIPTS}/blast_formatter_nano.pl -a outchunk.daa -o outchunk.ur100 -f 5,"6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore ppos qframe score salltitles"
    """
}

diamondBtabResults.collectFile(name: "${blastxDir}/${BASE}.ur100.btab").set { diamond_collect_ch }
diamondXmlResults.collectFile(name: "${blastxDir}/${BASE}.ur100.xml")
diamondLogResults.collectFile(name: "${blastxDir}/${BASE}.diamond.log")

process splitReads {
    input:
    val diamond_collect_ch from diamond_collect_ch
    
    output:
    stdout splitreads
     
    
    executor Executor
    """
      python3 ${SCRIPTS}/split_reads.py --fasta=$fastafile --blastx=${blastxDir}/${BASE}.ur100.btab
    """

}

unmapped_f = Channel.fromPath("${blastxDir}/${BASE}.unmapped.fasta")


process diamond_um {

    input:
    val sr from splitreads
    path unmapped_f

    output:
    file 'outchunk.ur100.btab' into diamondBtabResultsUm
    file 'outchunk.ur100.xml' into diamondXmlResultsUm
    file 'diamond.log' into diamondLogResultsUm
    stdout diamond_std_um into diamond_wait_um

    if ( Executor == 'local' ) {
       executor "local"
       maxForks 1
    }

    else if ( Executor == 'slurm' ) {
       clusterOptions "--ntasks-per-node $THREADS"
       executor "slurm"
    }


    script:
    if (unmapped_f.size() == 0)
    """ 
    touch ${blastxDir}/${BASE}.ur100.unmapped.btab 
    touch ${blastxDir}/${BASE}.ur100.unmapped.xml
    touch "outchunk.ur100.btab"
    touch "outchunk.ur100.xml"
    touch "diamond.log"
    """
    else
    """
    ## Run the BLASTx and produce an ASN file
    diamond blastx -q "$unmapped_f" \
        -d "${databaseDir}/diamond_1_2/uniref_1_2.dmnd" \
        -o outchunk \
        --evalue "${EVALUE}" \
        --threads "${THREADS}" \
        --block-size 4 \
        --index-chunks 3 \
        --salltitles \
        --frameshift 15 \
        --range-culling \
        --culling-overlap 50 \
        --range-cover 50 \
        --min-orf 30 \
        --masking seg \
        -k 1 \
        -f 100 \
        --log
        ${SCRIPTS}/blast_formatter_nano.pl -a outchunk.daa -o outchunk.ur100 -f 5,"6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore ppos qframe score salltitles"
    """
}

diamondBtabResultsUm.collectFile(name: "${blastxDir}/${BASE}.ur100.unmapped.btab").set { diamond_collect_ch_um }
diamondXmlResultsUm.collectFile(name: "${blastxDir}/${BASE}.ur100.unmapped.xml")
diamondLogResultsUm.collectFile(name: "${blastxDir}/${BASE}.diamond_um.log")

process splitReads_Um {
    input:
    val diamond_collect_ch_um from diamond_collect_ch_um

    output:
    stdout splitreads_um
    
    executor Executor
    """
      python3 ${SCRIPTS}/split_reads.py --fasta=${blastxDir}/${BASE}.unmapped.fasta --blastx=${blastxDir}/${BASE}.ur100.unmapped.btab --unmapped
    """

}


process postprocessDiamond {
    input:
    val init from init
    val sr from splitreads_um

    output:
    stdout postprocessDiamond_out

      if (Executor == 'local') {                                        
          executor "local"                                              
      }                                                                 
                                                                        
      else if (Executor == "slurm") {                                   
          clusterOptions "--ntasks-per-node $THREADS"                   
          executor "slurm"                                              
      }                                                                 
      script: 
      """
        cat ${blastxDir}/${BASE}.ur100.unmapped.btab >> ${blastxDir}/${BASE}.ur100.btab
        cat ${blastxDir}/${BASE}.ur100.unmapped.xml >> ${blastxDir}/${BASE}.ur100.xml
        ln -s ${BASE}.ur100.btab ${blastxDir}/functional_link.ur100.btab
        ln -s ${BASE}.ur100.xml  ${blastxDir}/functional_link.ur100.xml
      """
}


process taxonomic_assignment {
    input:
    val pp from postprocessDiamond_out

    output:
    stdout taxOut

    executor Executor
    
    """
    while [ ! -f $workingDir/taxonomic_identification/blastx/${BASE}.ur100.split.btab ]
    do
      sleep 5
    done
    
    ${SCRIPTS}/btab_2_tax_report_sensitive.pl --blastx=${blastxDir}/${BASE}.ur100.split.btab \
                                              --blastn=${blastnDir}/${BASE}.nt.btab_outlier_clean.btab \
    				                          --out=${taxDir}/taxonomic_results.txt \
    				                          $cutoff_flag
    python3 ${SCRIPTS}/nano_tax.py --blastx=${blastxDir}/${BASE}.ur100.split.btab \
                                   --database=${databaseDir} \
                                   --output=${taxDir}
    echo -n " # $WORKFLOW workflow complete ....... " | tee -a $logfile; date '+%H:%M:%S %Y-%m-%d' | tee -a $logfile
    """
}

process functional_assignments {
    input:
    val init from init
    val tx from taxOut

    output:
    stdout functional_assignments

    executor Executor
    
    """
    echo -n " # Launching $FWORKFLOW workflow ...... " | tee -a $logfile; date '+%H:%M:%S %Y-%m-%d' | tee -a $logfile
    while [ ! -f $workingDir/taxonomic_identification/blastx/${BASE}.ur100.split.btab ]
    do
      sleep 5
    done

    ${SCRIPTS}/functional_report_generation.py --fasta=$workingDir/taxonomic_identification/blastx/${BASE}.split.fasta \
                                               --urbtab=$workingDir/taxonomic_identification/blastx/${BASE}.ur100.split.btab \
                                               --go=${databaseDir}/go/go_network.txt \
                                               --out=${workingDir}/$FWORKFLOW/functional_assignments/functional_results.txt \
                                               --annotation=${databaseDir}/annotation_scores.pck \
                                               $ancestral_flag \
                                               $include_cc_flag \
                                               $cutoff_flag > ${workingDir}/$FWORKFLOW/functional_assignments.log 2>&1
    echo -n " # $FWORKFLOW workflow complete ....... " | tee -a $logfile; date '+%H:%M:%S %Y-%m-%d' | tee -a $logfile
    """
}

process seqscreen_tsv_report {
    input:
	val init from init
    val fa from functional_assignments
    
    output:
	stdout seqscreen_tsv_report

    executor Executor
    
    script:
    if (params.skip_report)
    """
    """
    else 
    """
    echo -n " # Launching $RWORKFLOW workflow ...... " | tee -a $logfile; date '+%H:%M:%S %Y-%m-%d' | tee -a $logfile
    ${SCRIPTS}/seqscreen_tsv_report.py \
                                    --taxonomy=${workingDir}/taxonomic_identification/taxonomic_assignment/taxonomic_results.txt \
                                    --functional=${workingDir}/functional_annotation/functional_assignments/functional_results.txt \
                                    --taxlookup=$databaseDir/taxonomy/taxa_lookup.txt \
                                    --funsocs=$databaseDir/funsocs \
                                    --fasta=$workingDir/taxonomic_identification/blastx/${BASE}.split.fasta \
                                    --mode=$mode \
                                    --out=$workingDir/$RWORKFLOW/${params.prefix}seqscreen_report.tsv
    """
}

process seqscreen_html_report {
    input:
        val seqscreen_tsv_report from seqscreen_tsv_report

    output:
        stdout seqscreen_html_report

    executor Executor

    script:
    if (params.keep_html_ont && !params.skip_report)
    """
    ${MODULES}/html_report_generation.sh \
        --report=$workingDir/$RWORKFLOW/${params.prefix}seqscreen_report.tsv \
        --fasta=$workingDir/taxonomic_identification/blastx/${BASE}.split.fasta \
        --blastx=$workingDir/taxonomic_identification/blastx/functional_link.ur100.xml \
        --version=$versionNum \
        --mode=$mode \
        --rflag=$rflag \
        --gonetwork=$databaseDir/go/go_network.txt \
        --out=$workingDir/$RWORKFLOW/${params.prefix}seqscreen_html_report/
    echo -n " # $RWORKFLOW workflow complete ....... " | tee -a $logfile; date '+%H:%M:%S %Y-%m-%d' | tee -a $logfile
    """
    else
    """
    """
}

process inference {
    input:
    val seqscreen_html_report

    output:
    stdout inference

    executor Executor
    """
    echo -n "# Launching inference workflow ...... " | tee -a $logfile; date '+%H:%M:%S %Y-%m-%d' | tee -a $logfile
    python3 ${SCRIPTS}/reference_inference.py --fasta=$fastafile \
                                              --output=$workingDir \
                                              --working=$taxDir/inference_working \
                                              --databases=$databaseDir \
                                              --threads=$THREADS \
                                              $online_flag
    echo -n "# inference complete ......." | tee -a $logfile; date '+%H:%M:%S %Y-%m-%d' | tee -a $logfile
    """
}


process format {
   input:
   val inference

   output:
   stdout format

   executor  Executor

   script:
   if (params.skip_report)
   """
   """
   else
   """
   echo -n " # Launching format workflow ...... " | tee -a $logfile; date '+%H:%M:%S %Y-%m-%d' | tee -a $logfile
   python3 ${SCRIPTS}/format.py --report $workingDir/report_generation \
                                --format=${params.format} \
                                --mode $mode \
                                --databases=$databaseDir \
                                --taxonomy_confidence_threshold=${params.taxonomy_confidence_threshold} \
                                --filter-taxon ${params.filter_taxon} \
                                --keep-taxon ${params.keep_taxon}
   echo -n " # format complete ....... " | tee -a $logfile; date '+%H:%M:%S %Y-%m-%d' | tee -a $logfile
   """
 }

process cleanup {
    input:
    val format
    
    output:
    stdout cleanup

    script:
        """
        echo -n " # Launching cleanup workflow ...... " | tee -a $logfile; date '+%H:%M:%S %Y-%m-%d' | tee -a $logfile
        rm -r $workingDir/taxonomic_identification/blastn
        rm -r "${blastxDir}/${BASE}.unmapped.fasta.unmapped.fasta"
        echo -n " # cleanup complete ....... " | tee -a $logfile; date '+%H:%M:%S %Y-%m-%d' | tee -a $logfile
        """
}

