#!/usr/bin/env nextflow

params.databases = ''
params.working = ''
params.fasta = ''
params.version = ''
params.log = '/dev/null'
params.help = false
params.slurm = false
params.prefix = ''
params.evalue=''
params.sensitive = ''
mode = 'fast'
Executor = 'local'
params.hmmscan = false
params.ancestral = false
params.bitscore = ""
params.blastn = false
params.taxlimit = ""
params.splitby = ""
params.includecc = false
bsat_flag = ''

def usage() {
    log.info ''
    log.info 'Usage: nextflow run report_generation.nf --fasta /Path/to/input.fasta --version "1.0XX" --working /Path/to/working_directory --databases /Path/to/databases [--slurm] [--log /Path/to/log_file.txt]'
    log.info '  --fasta     Path to the input FASTA file'
    log.info '  --version   Verison number for the seqscreen pipeline'
    log.info '  --working   Path to the seqscreen output working directory'
    log.info '  --databases Path to the database directory'
    log.info '  --slurm     Submit modules in this workflow to run on a SLURM grid (Default = run locally)'
    log.info '  --ancestral     Include all ancestral GO terms in output'
    log.info '  --hmmscan       Run hmmscan on input sequences'
    log.info "  --evalue        Cutoff to use for blastx/diamond"
    log.info "  --prefix   Add argument to beginning of the output files"
    log.info '  --log       Path to a log file to tee log info to (Default: no log file)'
    log.info '  --help      Print this help message out'
    log.info '  --sensitive Use SeqScreen sensitive mode (old default mode)'
    log.info ''
    exit 1
}

if (params.help) {
    usage()
}

if (params.slurm) {
    Executor = 'slurm'
}

if (!params.fasta) {
    log.error "Missing argument for --fasta option"
    usage()
}

if (!params.version) {
    log.error "Missing argument for --version option"
    usage()
}

if (!params.working) {
    log.error "Missing argument for --working option"
    usage()
}

if (!params.databases) {
    log.error "Missing argument for --databases option"
    usage()
}

rflag = ''
if (params.hmmscan) {
    rflag  = "hmmscan"
}
if (params.ancestral) {
   if(rflag) {
      
       rflag = rflag + ",ancestral"
   }
   else {
       rflag = "ancestral"
   }
}
if (params.evalue != "") {
   if(rflag) {
       rflag = rflag + ",evalue:" + params.evalue
   }
   else {
       rflag = "evalue:" + params.evalue
   }
}

if (params.bitscore != "") {
    if(rflag) {
        rflag = rflag + ",bitscore:" + params.bitscore
    }else{
        rflag = "bitscore:" + params.bitscore
    }
}

if (params.blastn){
    if(rflag){
        rflag = rflag + ",blastn"
    }else{
        rflag = 'blastn'
    }
}

if (params.taxlimit != ""){
    if(rflag){
        rflag = rflag + ",taxlimit:" + params.taxlimit
    }else{
        rflag = "taxlimit:" + params.taxlimit
    }
}

if (params.includecc){
    if(rflag){
        rflag = rflag + ",includecc"
    }else{
        rflag = 'includecc'
    }
}

if (params.splitby != ""){
    if(rflag){
        rflag = rflag + ",splitby:" + params.splitby
    }else{
        rflag = "splitby:" + params.splitby
    }
}


if(! rflag)
{
   rflag = "None"
}
if (params.sensitive) {
    mode = 'sensitive'
}

databasesDir = file(params.databases)
workingDir = file(params.working)
fastafile = file(params.fasta)
versionNum = params.version
logfile = file(params.log)

MODULES =  "$workflow.projectDir/../modules"
SCRIPTS = "$workflow.projectDir/../scripts"
WORKFLOW="report_generation"

if (mode == "sensitive"){
    bsat_flag = "--bsat=${workingDir}/seqmapper/threats_by_blacklist.txt"
}


process Create_Working_Directories {
    output:
    stdout create

    """
    echo -n " # Launching $WORKFLOW workflow ...... " | tee -a $logfile; date '+%H:%M:%S %Y-%m-%d' | tee -a $logfile
    echo "The mode is: $mode"
    mkdir -p $workingDir
    if [ -d $workingDir/$WORKFLOW ]; then rm -rf $workingDir/$WORKFLOW; fi;
    mkdir -p $workingDir/$WORKFLOW
    """
}

process seqscreen_tsv_report {
    input:
	val create from create
    
    output:
	stdout seqscreen_tsv_report

    executor Executor
    
    """
    ${SCRIPTS}/seqscreen_tsv_report.py \
                                    --taxonomy=${workingDir}/taxonomic_identification/taxonomic_assignment/taxonomic_results.txt \
                                    --functional=${workingDir}/functional_annotation/functional_assignments/functional_results.txt \
                                    --taxlookup=$databasesDir/taxonomy/taxa_lookup.txt \
                                    --funsocs=$databasesDir/funsocs \
                                    --fasta=${fastafile} \
                                    --mode=$mode \
                                    $bsat_flag \
                                    --out=$workingDir/$WORKFLOW/${params.prefix}seqscreen_report.tsv
    """
}

process seqscreen_html_report {
    input:
        val seqscreen_tsv_report from seqscreen_tsv_report

    output:
        stdout seqscreen_html_report

    executor Executor

    """
    ${MODULES}/html_report_generation.sh \
        --report=$workingDir/$WORKFLOW/${params.prefix}seqscreen_report.tsv \
        --fasta=${fastafile} \
        --blastx=$workingDir/taxonomic_identification/blastx/functional_link.ur100.xml \
        --version=$versionNum \
        --mode=$mode \
        --rflag=$rflag \
        --gonetwork=$databasesDir/go/go_network.txt \
        --out=$workingDir/$WORKFLOW/${params.prefix}seqscreen_html_report/
    """
}


create.subscribe { print "$it" }
seqscreen_tsv_report.subscribe { print "$it" }
seqscreen_html_report.subscribe { print "$it" }


