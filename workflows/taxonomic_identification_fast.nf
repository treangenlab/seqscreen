#!/usr/bin/env nextflow

params.fasta = ''
params.working = ''
params.databases = ''
params.threads = 1
params.evalue = 10
params.taxlimit = 25
params.splitby = 100000
params.log = '/dev/null'
params.help = false
params.slurm = false
Executor = 'local'

MODULES =  "$workflow.projectDir/../modules"
SCRIPTS =  "$workflow.projectDir/../scripts"

def usage() {
    log.info ""
    log.info "Usage: nextflow run taxonomic_identification.nf --fasta /Path/to/infile.fasta --databases /Path/to/databases --working /Path/to/working_directory [--evalue 10] [--threads 4] [--slurm]  [--log /Path/to/log.txt]"
    log.info "  --fasta     Path to the input NT FASTA file"
    log.info "  --databases Path to the database directory"
    log.info "  --working   Path to the output working directory"
    log.info "  --evalue    E value cut-off (Default=10)"
    log.info "  --threads   Number of threads to use (Default=1)"
    log.info "  --splitby   Number of fasta sequences in each chunk"
    log.info "  --taxlimit  Maximum number of taxIDs to output for a single input"
    log.info '  --slurm     Submit modules in this workflow to run on a SLURM grid (Default = run locally)'
    log.info "  --log       Where to write log file (Default=no log)"
    log.info "  -h, --help  Print this help message out"
    log.info ""
    exit 1
}

if (params.help) {
    usage()
}

if (params.slurm) {
    Executor = 'slurm'
}

if (!params.fasta) {
    log.error "Missing argument for --fasta option"
    usage()
}

if (!params.working) {
    log.error "Missing argument for --working option"
    usage()
}

if (!params.databases) {
    log.error "Missing argument for --databases option"
    usage()
}

fastafile = file(params.fasta)
workingDir = file(params.working)
databaseDir = file(params.databases)
logfile = file(params.log)

BASE = fastafile.getName()
THREADS = params.threads
EVALUE = params.evalue
WORKFLOW = "taxonomic_identification"

blastxDir = file("$workingDir/$WORKFLOW/blastx")
taxDir = file("$workingDir/$WORKFLOW/taxonomic_assignment")
outlierDir = file("$workingDir/$WORKFLOW/outlier_detection")

translatedFile = "$workingDir/initialize/six_frame_translation/${BASE}.translated.fasta"


process Create_Working_Directories {
    output:
    stdout create

    """
    mkdir -p $workingDir/$WORKFLOW/
    if [ -d $blastxDir ]; then rm -rf $blastxDir; fi;
    if [ -d $taxDir ]; then rm -rf $taxDir; fi;
    
    mkdir $blastxDir
    mkdir $taxDir
    """
}

process Initialize {
    input:
    val create from create

    output:
    stdout init

    executor Executor

    """
    echo -n " # Launching $WORKFLOW workflow ...... " | tee -a $logfile; date '+%H:%M:%S %Y-%m-%d' | tee -a $logfile
    """
}


Channel
    .fromPath(fastafile)
    .splitFasta(by: params.splitby, file: true)
    .set { fasta_ch }

process diamond {

    input:
    val	init from init
    file 'chunk' from fasta_ch

    output:
    file 'outchunk.ur100.btab' into diamondBtabResults
    file 'outchunk.ur100.xml' into diamondXmlResults
    stdout diamond_std into diamond_wait

    if ( Executor == 'local' ) {
       executor "local"
       maxForks 1
    }

    else if ( Executor == 'slurm' ) {
       clusterOptions "--ntasks-per-node $THREADS"
       executor "slurm"
    }

    script:
    """
    echo "running diamond on ${chunk}"

    ## Run the BLASTx and produce an ASN file
    diamond blastx -q chunk \
        -d "${databaseDir}/diamond/uniref.mini.dmnd" \
        -o outchunk \
        --evalue "${EVALUE}" \
        --threads "${THREADS}" \
        --block-size 200 \
        --index-chunks 1 \
        --salltitles \
        --more-sensitive \
        --min-orf 10 \
        --masking 0 \
        --top 5 \
        -f 100 
        ${SCRIPTS}/blast_formatter_fast.pl -a outchunk.daa -o outchunk.ur100 -f 5,"6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore ppos qframe score salltitles"
    """
}

diamondBtabResults.into{ postprocessDiamond_trigger; diamondBtabResults_coll}
diamondBtabResults_coll.collectFile(name: "${blastxDir}/${BASE}.ur100.btab").set { diamond_collect_ch }
diamondXmlResults.collectFile(name: "${blastxDir}/${BASE}.ur100.xml")

process postprocessDiamond {
    input: 
    file "*outchunk.ur100.btab" from postprocessDiamond_trigger.collect()

    output:
    stdout postprocessDiamond_out

      if (Executor == 'local') {                                        
          executor "local"                                              
      }                                                                 
                                                                        
      else if (Executor == "slurm") {                                   
          clusterOptions "--ntasks-per-node $THREADS"                   
          executor "slurm"                                              
      }                                                                 
      script: 
      """
        ln -s ${BASE}.ur100.btab ${blastxDir}/functional_link.ur100.btab
        ln -s ${BASE}.ur100.xml  ${blastxDir}/functional_link.ur100.xml
      """
}

 process centrifuge {                                                   
      input:                                                            
      val init from init
      
      output:                                                           
      stdout centrifugeOut                                              
                                                                        
      if (Executor == 'local') {                                        
          executor "local"                                              
      }                                                                 
                                                                        
      else if (Executor == "slurm") {                                   
          clusterOptions "--ntasks-per-node $THREADS"                   
          executor "slurm"                                              
      }                                                                 
                                                                        
      """                                                                
      ${MODULES}/centrifuge.sh --fasta=$fastafile \
                      --database=${databaseDir}/centrifuge/abv \
                      --out=${blastxDir}/${BASE}.centrifuge \
                       --threads=${THREADS} \
      """                                                               
}                                                                     
                                                                        

process merge_centrifuge_diamond {
    
    input:
    val centrifugeOut
    val postprocessDiamond_out
    
    output:
    stdout merged_output
    
    executor Executor                                                 
                                                                        
     """                                                                
     python3 ${SCRIPTS}/consolidatedtax_2_taxreport_fast.py --diamond=${blastxDir}/${BASE}.ur100.btab \
                         --centrifuge=${blastxDir}/${BASE}.centrifuge \
                         --out=${taxDir}/taxonomic_results.txt \
                         --cutoff=1 \
                         --taxlimit=${params.taxlimit}
     """                                                                
}

create.subscribe { print "$it" }
init.subscribe { print "$it" }
centrifugeOut.subscribe { print "$it" }
merged_output.subscribe { print "$it"}
